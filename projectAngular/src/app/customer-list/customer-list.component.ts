import { Component, OnInit } from '@angular/core';
import {CustomersService} from '../Service/customers.service';
import {Customer} from '../Model/Customer';

@Component({
  selector: 'app-customer-list',
  templateUrl: './customer-list.component.html',
  styleUrls: ['./customer-list.component.css']
})
export class CustomerListComponent implements OnInit {
helloworld = 'Hello world!';
 customers: Customer[];
  constructor(
    // tslint:disable-next-line:no-shadowed-variable
    private CustomersService: CustomersService
  ) { }

  ngOnInit(): void {
    this.CustomersService.getAllCustomer().subscribe({ next: customers => this.customers = customers.body});
  }

}

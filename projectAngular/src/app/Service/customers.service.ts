import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CustomersService {
  // tslint:disable-next-line:variable-name
  private api_url = 'http://localhost:9999/api/custome';
  constructor(
    private http: HttpClient
  ) {}
  getAllCustomer(): Observable<any> {

    return this.http.get(this.api_url, { observe: 'response' });
  }

}

package colval.web.demodbfirst.services;

import colval.web.demodbfirst.Models.entities.Customer;

import java.util.List;
import java.util.Optional;

public interface customerService {

    Customer create(Customer customer);

    Optional<Customer> readOne(long id);

    List<Customer> readAll();

    void delete(long id);

    List<Customer> getAllCustomerSortedByLastName();

    Customer updateFirstNameAndLastName(long customerId, String firstName, String lastName);

}

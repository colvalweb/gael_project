package colval.web.demodbfirst.repositories;

import colval.web.demodbfirst.Models.entities.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface customerRepository extends JpaRepository<Customer, Long> {

    List<Customer> findAllCustomerSortedByLastName();


}

package com.example.demojpa.repositories;

import com.example.demojpa.entities.Address;
import org.springframework.data.jpa.repository.JpaRepository;


public interface AddressRepository extends JpaRepository<Address, Long> {


}

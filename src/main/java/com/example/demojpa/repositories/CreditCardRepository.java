package com.example.demojpa.repositories;

import com.example.demojpa.entities.CreditCard;

import org.springframework.data.jpa.repository.JpaRepository;


public interface CreditCardRepository extends JpaRepository<CreditCard, Long> {
}

package com.example.demojpa.repositories;

import com.example.demojpa.entities.City;
import org.springframework.data.jpa.repository.JpaRepository;


public interface CityRepository extends JpaRepository<City, Long> {
}

package com.example.demojpa;

//Ajouter package des entities

import com.example.demojpa.entities.*;
import com.example.demojpa.entities.enumeration.Continent;
import com.example.demojpa.entities.enumeration.Gender;

//Ajouter package des services
import com.example.demojpa.services.impl.CityServiceImpl;
import com.example.demojpa.services.impl.UserServiceImpl;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


// Annotation des beans
import org.springframework.context.annotation.Bean;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;

//IoC par mutation
import org.springframework.beans.factory.annotation.Autowired;


// Pour importations des logs
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SpringBootApplication
public class DemojpaApplication {

    private final Logger log = LoggerFactory.getLogger(DemojpaApplication.class);

    @Autowired
    private UserServiceImpl userService;

    @Autowired
    private CityServiceImpl cityService;


    public static void main(String[] args) {

        SpringApplication.run(DemojpaApplication.class, args);
    }

    //Ajoute dans le contexte de l'application et pourra l'executer
    @Bean
    void crudUser() {

        //save
        Address address = new Address();

        Role role = new Role();
        role.setNames("ADMIN");
        HashSet<Role> roles = new HashSet<>();
        roles.add(role);

        CreditCard creditCard = new CreditCard();
        creditCard.setCardHolder("Stephane Tremblay");
        HashSet<CreditCard> creditCards = new HashSet<>();
        creditCards.add(creditCard);

        address.setNames("Canada, ontario");
        User user = new User();
        user.setLastName("Justin");
        user.setFirstName("Trudeau");
        user.setAddress(address);
        user.setRoleSet(roles);
        user.setGender(Gender.Male);
        user.setCreditCards(creditCards);
        user = userService.create(user);
        log.info("****************** save user : {}", user);

        //read one
        Optional<User> userRecover = userService.readOne(user.getId());
        if (userRecover.isPresent()) {
            log.info("****************** Read one user : {}", user);
        }

        //readAll
        List<User> users = userService.readAll();
        log.info("****************** read all users : {}", users);

        //update
        user.setGender(Gender.Female);
        user.setFirstName("christelle");
        user.setLastName("Jonhson");
        userService.create(user);

        //delete
        //userService.delete(user.getId());
        log.info("****************** delete user : {}", user.getId());
    }


    @Bean
    void crudCity() {

        //save
        Country country = new Country();
        country.setCapital(11);
        country.setName("Canada");
        country.setContinent(Continent.NorthAmerica);

        City city = new City();
        city.setCountry(country);
        city.setName("ontario");
        city.setPopulation(999);
        city = cityService.create(city);
        log.info("****************** save city {} :", city);

        //read one
        Optional<City> cityRecover = cityService.readOne(city.getId());
        cityRecover.ifPresent(value -> log.info("****************** read one {} :", value));

        //read all
        List<City> cities = cityService.readAll();
        log.info("****************** read all {} :", cities);

        //update
        city.setCountry(country);
        city.setName("quebec");
        city.setPopulation(111);
        city = cityService.create(city);
        log.info("****************** update city {} :", city);

        //delete
        //cityService.delete(city.getId());
        log.info("****************** delete city : {}", city.getId());
    }

}

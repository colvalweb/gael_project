package com.example.demojpa.services;

import com.example.demojpa.entities.Address;


import java.util.List;
import java.util.Optional;


public interface AddressService {

    Address create(Address address);

    Optional<Address> readOne(Long id);

    List<Address> readAll();

    void delete(Long id);

}

package com.example.demojpa.services;

import com.example.demojpa.entities.Country;


import java.util.List;
import java.util.Optional;


public interface CountryService {

    Country create(Country country);

    Optional<Country> readOne(Long id);

    List<Country> readAll();

    void delete(Long id);

}

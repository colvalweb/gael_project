package com.example.demojpa.services;

import com.example.demojpa.entities.Role;


import java.util.List;
import java.util.Optional;


public interface RoleService {

    Role create(Role role);

    Optional<Role> readOne(Long id);

    List<Role> readAll();

    void delete(Long id);

}

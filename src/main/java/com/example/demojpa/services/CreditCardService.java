package com.example.demojpa.services;

import com.example.demojpa.entities.CreditCard;


import java.util.List;
import java.util.Optional;


public interface CreditCardService {

    CreditCard create(CreditCard creditCard);

    Optional<CreditCard> readOne(Long id);

    List<CreditCard> readAll();

    void delete(Long id);

}

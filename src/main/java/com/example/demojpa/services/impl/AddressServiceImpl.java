package com.example.demojpa.services.impl;

import com.example.demojpa.entities.Address;
import com.example.demojpa.repositories.AddressRepository;
import com.example.demojpa.services.AddressService;

import java.util.List;
import java.util.Optional;


public class AddressServiceImpl implements AddressService {

    private final AddressRepository addressRepository;

    public AddressServiceImpl(AddressRepository addressRepository) {
        this.addressRepository = addressRepository;
    }

    @Override
    public Address create(Address address) {
        return addressRepository.save(address);
    }

    @Override
    public Optional<Address> readOne(Long id) {
        return addressRepository.findById(id);
    }

    @Override
    public List<Address> readAll() {
        return addressRepository.findAll();
    }

    @Override
    public void delete(Long id) {
        addressRepository.deleteById(id);
    }
}

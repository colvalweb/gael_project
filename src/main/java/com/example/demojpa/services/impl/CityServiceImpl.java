package com.example.demojpa.services.impl;

import com.example.demojpa.entities.City;
import com.example.demojpa.repositories.CityRepository;
import com.example.demojpa.services.CityService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


@Service
public class CityServiceImpl implements CityService {

    private final CityRepository cityRepository;

    public CityServiceImpl(CityRepository cityRepository) {
        this.cityRepository = cityRepository;
    }

    @Override
    public City create(City city) {
        return cityRepository.save(city);
    }

    @Override
    public Optional<City> readOne(Long id) {
        return cityRepository.findById(id);
    }

    @Override
    public List<City> readAll() {
        return cityRepository.findAll();
    }

    @Override
    public void delete(Long id) {
        cityRepository.deleteById(id);
    }
}

package com.example.demojpa.services.impl;

import com.example.demojpa.entities.Role;
import com.example.demojpa.repositories.RoleRepository;
import com.example.demojpa.services.RoleService;

import java.util.List;
import java.util.Optional;


public class RoleServiceImpl implements RoleService {

    private final RoleRepository roleRepository;

    public RoleServiceImpl(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    @Override
    public Role create(Role role) {
        return roleRepository.save(role);
    }

    @Override
    public Optional<Role> readOne(Long id) {
        return roleRepository.findById(id);
    }

    @Override
    public List<Role> readAll() {
        return roleRepository.findAll();
    }

    @Override
    public void delete(Long id) {
        roleRepository.deleteById(id);
    }
}

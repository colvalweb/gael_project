package com.example.demojpa.services.impl;

import com.example.demojpa.entities.CreditCard;
import com.example.demojpa.repositories.CreditCardRepository;
import com.example.demojpa.services.CreditCardService;

import java.util.List;
import java.util.Optional;


public class CreditCardServiceImpl implements CreditCardService {

    private final CreditCardRepository creditCardRepository;

    public CreditCardServiceImpl(CreditCardRepository creditCardRepository) {
        this.creditCardRepository = creditCardRepository;
    }

    @Override
    public CreditCard create(CreditCard creditCard) {
        return creditCardRepository.save(creditCard);
    }

    @Override
    public Optional<CreditCard> readOne(Long id) {
        return creditCardRepository.findById(id);
    }

    @Override
    public List<CreditCard> readAll() {
        return creditCardRepository.findAll();
    }

    @Override
    public void delete(Long id) {
        creditCardRepository.deleteById(id);
    }
}

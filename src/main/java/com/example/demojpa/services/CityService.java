package com.example.demojpa.services;

import com.example.demojpa.entities.City;


import java.util.List;
import java.util.Optional;


public interface CityService {

    City create(City city);

    Optional<City> readOne(Long id);

    List<City> readAll();

    void delete(Long id);

}

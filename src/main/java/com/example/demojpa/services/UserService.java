package com.example.demojpa.services;

import com.example.demojpa.entities.User;


import java.util.List;
import java.util.Optional;


public interface UserService {

    User create(User user);

    Optional<User> readOne(Long id);

    List<User> readAll();

    void delete(Long id);

}

package com.example.demojpa.entities;

import javax.persistence.*;
import java.io.Serializable;


@Entity
@Table(name = "address")
public class Address implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String names;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNames() {
        return names;
    }

    public void setNames(String names) {
        this.names = names;
    }

    @Override
    public String toString() {
        return "Address{" +
                "id=" + id +
                ", names='" + names + '\'' +
                '}';
    }
}

package com.example.demojpa.entities.enumeration;

/**
 * The Continent enumeration.
 */
public enum Continent {
    Asia, Europe, NorthAmerica, Africa, Oceania, Antarctica, SouthAmerica
}

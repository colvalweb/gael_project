package com.colval.web.td0p2.Fel;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Td0p2Application {

    public static void main(String[] args) {
        SpringApplication.run(Td0p2Application.class, args);
    }

}

package com.colval.web.td0p2.Model;

public class Compte {
    private int numero;
    private String type;
    private double soldeInitial;
    private double soldeActuel;
    public Client client;

    public Compte(int numero, String type, double soldeInitial, double soldeActuel, Client client) {
        this.numero = numero;
        this.type = type;
        this.soldeInitial = soldeInitial;
        this.soldeActuel = soldeActuel;
        this.client = client;
    }

    public int getNumero() {
        return numero;
    }

    public String getType() {
        return type;
    }

    public double getSoldeInitial() {
        return soldeInitial;
    }

    public double getSoldeActuel() {
        return soldeActuel;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setSoldeInitial(double soldeInitial) {
        this.soldeInitial = soldeInitial;
    }

    public void setSoldeActuel(double soldeActuel) {
        this.soldeActuel = soldeActuel;
    }

    @Override
    public String toString() {
        return "Compte{" +
                "numero=" + numero +
                ", type=" + type +
                ", soldeInitial=" + soldeInitial +
                ", soldeActuel=" + soldeActuel +
                '}';
    }
}
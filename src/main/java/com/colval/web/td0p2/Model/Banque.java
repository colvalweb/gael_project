package com.colval.web.td0p2.Model;

import java.util.*;


public class Banque {
    private String nom;
    private String adresse;
    public List<Personne> personnes;

    public Banque(String nom, String adresse) {
        this.nom = nom;
        this.adresse = adresse;
        this.personnes = new ArrayList<>();

    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    @Override
    public String toString() {
        return "Banque{" +
                "nom=" + nom +
                ", adresse=" + adresse +
                '}';
    }
}

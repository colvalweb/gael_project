package com.colval.web.td0p2.Model;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Gestionnaire extends Personne {
    public static int autoId = 1;
    private int gestionnaireID;
    private int identifiant;
    private int motDePasse;
    private int numeroBureau;
    public List<Client> clients;

    public Gestionnaire(String nom, String prenom, String telephone, String email, String sexe, Adresse adresse, int numeroBureau) {
        super(nom, prenom, telephone, email, sexe, adresse);
        this.gestionnaireID = autoId++;
        this.numeroBureau = numeroBureau;
        this.identifiant = 31012021;
        this.motDePasse = 2021;
        this.clients = new ArrayList<>();
    }

    public void setIdentifiant(int identifiant) {
        this.identifiant = identifiant;
    }

    public void setMotDePasse(int motDePasse) {
        this.motDePasse = motDePasse;
    }

    public int getIdentifiant() {
        return identifiant;
    }

    public int getMotDePasse() {
        return motDePasse;
    }

    public int getGestionnaireID() {
        return gestionnaireID;
    }

    public int getNumeroBureau() {
        return numeroBureau;
    }

    public void setNumeroBureau(int numeroBureau) {
        this.numeroBureau = numeroBureau;
    }

    @Override
    public String toString() {
        return "Gestionnaire{" +
                "gestionnaireID=" + gestionnaireID +
                ", numeroBureau=" + numeroBureau +
                '}';
    }
}

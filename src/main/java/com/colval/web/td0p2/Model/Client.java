package com.colval.web.td0p2.Model;

import java.util.ArrayList;
import java.util.List;

public class Client extends Personne {
    public static int autoId = 1;
    private int numeroClient;
    private double salaire;
    private int codeConfidentiel;
    private String statutMatrimonial;
    private String dateNaissance;
    public Gestionnaire gestionnaire;
    public List<Compte> comptes;

    public Client(String nom, String prenom, String telephone, String email, String sexe, Adresse adresse, double salaire, String statutMatrimonial, int codeConfidentiel, String dateNaissance, Gestionnaire gestionnaire) {
        super(nom, prenom, telephone, email, sexe, adresse);
        this.numeroClient = autoId++;
        this.salaire = salaire;
        this.statutMatrimonial = statutMatrimonial;
        this.codeConfidentiel = codeConfidentiel;
        this.dateNaissance = dateNaissance;
        this.gestionnaire = gestionnaire;
        this.comptes = new ArrayList<>();
    }

    public int getNumeroClient() {
        return numeroClient;
    }

    public double getSalaire() {
        return salaire;
    }

    public int getCodeConfidentiel() {
        return codeConfidentiel;
    }

    public String getStatutMatrimonial() {
        return statutMatrimonial;
    }

    public String getDateNaissance() {
        return dateNaissance;
    }

    public void setNumeroClient(int numeroClient) {
        this.numeroClient = numeroClient;
    }

    public void setSalaire(double salaire) {
        this.salaire = salaire;
    }

    public void setCodeConfidentiel(int codeConfidentiel) {
        this.codeConfidentiel = codeConfidentiel;
    }

    public void setStatutMatrimonial(String statutMatrimonial) {
        this.statutMatrimonial = statutMatrimonial;
    }

    public void setDateNaissance(String dateNaissance) {
        this.dateNaissance = dateNaissance;
    }

    @Override
    public String toString() {
        return "Client{" +
                "numeroClient=" + numeroClient +
                ", salaire=" + salaire +
                ", codeConfidentiel=" + codeConfidentiel +
                ", statutMatrimonial='" + statutMatrimonial + '\'' +
                ", dateNaissance='" + dateNaissance + '\'' +
                ", gestionnaire=" + gestionnaire.toString() +
                '}';
    }
}

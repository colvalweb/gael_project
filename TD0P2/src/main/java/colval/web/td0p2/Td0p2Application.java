package colval.web.td0p2;

import colval.web.td0p2.Model.*;
import colval.web.td0p2.Services.personneServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.List;
import java.util.Optional;

@SpringBootApplication
public class Td0p2Application {

    private final Logger log = LoggerFactory.getLogger(Td0p2Application.class);

    @Autowired
    private personneServiceImpl personneService;

    public static void main(String[] args) {

        SpringApplication.run(Td0p2Application.class, args);
    }

    @Bean
    void crudpersonne() {

        //save
        Banque banque = new Banque();

        banque.setNom("Mini-Bank");

        Adresse adresse = new Adresse();

        adresse.setRue("rue Champlain");
        adresse.setNumeroCivique("169");
        adresse.setVille("valleyfield");
        adresse.setCodePostal("J6T 1X6");

        Personne personne = new Personne();
        personne.setNom("Gael");
        personne.setPrenom("Onouk");
        personne.setTelephone("4383992568");
        personne.setEmail("gaelonouk@gmail.com");
        personne.setSexe("masculin");
        personne.setAdresse(adresse);
        personne.setBanque(banque);
        personne = personneService.create(personne);
        log.info("****************** save personne : {}", personne);

        //read one
        Optional<Personne> personneRecover = personneService.readOne(personne.getId());
        if (personneRecover.isPresent()) {
            log.info("****************** Read one personne : {}", personne);
        }

        //readAll
        List<Personne> personnes = personneService.readAll();
        log.info("****************** read all personnes : {}", personnes);


    }
}

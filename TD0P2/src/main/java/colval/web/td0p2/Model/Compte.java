package colval.web.td0p2.Model;

import javax.persistence.*;

@Entity
@Table(name = "compte")
public class Compte {
    @javax.persistence.Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long Id;
    private int numero;
    private String type;
    private double soldeInitial;
    private double soldeActuel;

    public long getId() {
        return Id;
    }

    public void setId(long id) {
        Id = id;
    }

    public int getNumero() {
        return numero;
    }

    public String getType() {
        return type;
    }

    public double getSoldeInitial() {
        return soldeInitial;
    }

    public double getSoldeActuel() {
        return soldeActuel;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setSoldeInitial(double soldeInitial) {
        this.soldeInitial = soldeInitial;
    }

    public void setSoldeActuel(double soldeActuel) {
        this.soldeActuel = soldeActuel;
    }

    @Override
    public String toString() {
        return "Compte{" +
                "numero=" + numero +
                ", type=" + type +
                ", soldeInitial=" + soldeInitial +
                ", soldeActuel=" + soldeActuel +
                '}';
    }
}
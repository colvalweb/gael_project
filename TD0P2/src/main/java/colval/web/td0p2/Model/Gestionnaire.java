package colval.web.td0p2.Model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

@Entity
@Table(name = "gestionnaire")
public class Gestionnaire extends Personne {
    @javax.persistence.Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long Id;
    private int identifiant;
    private int motDePasse;
    private int numeroBureau;
    @OneToMany(cascade = CascadeType.ALL)
    private List<Client> clients;

    @Override
    public long getId() {
        return Id;
    }

    @Override
    public void setId(long id) {
        Id = id;
    }

    public void setIdentifiant(int identifiant) {
        this.identifiant = identifiant;
    }

    public void setMotDePasse(int motDePasse) {
        this.motDePasse = motDePasse;
    }

    public int getIdentifiant() {
        return identifiant;
    }

    public int getMotDePasse() {
        return motDePasse;
    }

    public int getNumeroBureau() {
        return numeroBureau;
    }

    public void setNumeroBureau(int numeroBureau) {
        this.numeroBureau = numeroBureau;
    }

    @Override
    public String toString() {
        return "Gestionnaire{" +
                "gestionnaireID=" + Id +
                ", numeroBureau=" + numeroBureau +
                '}';
    }
}

package colval.web.td0p2.Model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "client")
public class Client extends Personne {
    @javax.persistence.Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long Id;
    private int numeroClient;
    private double salaire;
    private int codeConfidentiel;
    private String statutMatrimonial;
    private String dateNaissance;
    @OneToMany(cascade = CascadeType.ALL)
    private List<Compte> comptes;

    public void setComptes(List<Compte> comptes) {
        this.comptes = comptes;
    }

    @Override
    public long getId() {
        return Id;
    }

    @Override
    public void setId(long id) {
        Id = id;
    }

    public int getNumeroClient() {
        return numeroClient;
    }

    public double getSalaire() {
        return salaire;
    }

    public int getCodeConfidentiel() {
        return codeConfidentiel;
    }

    public String getStatutMatrimonial() {
        return statutMatrimonial;
    }

    public String getDateNaissance() {
        return dateNaissance;
    }

    public void setNumeroClient(int numeroClient) {
        this.numeroClient = numeroClient;
    }

    public void setSalaire(double salaire) {
        this.salaire = salaire;
    }

    public void setCodeConfidentiel(int codeConfidentiel) {
        this.codeConfidentiel = codeConfidentiel;
    }

    public void setStatutMatrimonial(String statutMatrimonial) {
        this.statutMatrimonial = statutMatrimonial;
    }

    public void setDateNaissance(String dateNaissance) {
        this.dateNaissance = dateNaissance;
    }

    @Override
    public String toString() {
        return "Client{" +
                "numeroClient=" + numeroClient +
                ", salaire=" + salaire +
                ", codeConfidentiel=" + codeConfidentiel +
                ", statutMatrimonial='" + statutMatrimonial + '\'' +
                ", dateNaissance='" + dateNaissance + '\'' +
                '}';
    }
}

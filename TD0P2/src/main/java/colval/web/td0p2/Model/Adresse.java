package colval.web.td0p2.Model;

import javax.persistence.*;

@Entity
@Table(name = "adresse")
public class Adresse {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long Id;
    private String rue;
    private String numeroCivique;
    private String codePostal;
    private String ville;

    public long getId() {
        return Id;
    }

    public void setId(long id) {
        Id = id;
    }

    public String getRue() {
        return rue;
    }

    public String getNumeroCivique() {
        return numeroCivique;
    }

    public String getCodePostal() {
        return codePostal;
    }

    public String getVille() {
        return ville;
    }

    public void setRue(String rue) {
        this.rue = rue;
    }

    public void setNumeroCivique(String numeroCivique) {
        this.numeroCivique = numeroCivique;
    }

    public void setCodePostal(String codePostal) {
        this.codePostal = codePostal;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    @Override
    public String toString() {
        return "Adresse{" +
                "rue='" + rue + '\'' +
                ", numeroCivique='" + numeroCivique + '\'' +
                ", codePostal='" + codePostal + '\'' +
                ", ville='" + ville +
                '}';
    }
}

package colval.web.td0p2.Services;

import colval.web.td0p2.Model.Personne;

import java.util.List;
import java.util.Optional;

public interface personneService {
    Personne create(Personne Personne);

    Optional<Personne> readOne(Long id);

    List<Personne> readAll();
}

package colval.web.td0p2.Services;

import colval.web.td0p2.Model.Personne;
import colval.web.td0p2.Repositories.personneRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class personneServiceImpl implements personneService {

    private final personneRepository personneRepository;

    public personneServiceImpl(personneRepository personneRepository) {
        this.personneRepository = personneRepository;

    }

    @Override
    public Personne create(Personne Personne) {
        return personneRepository.save(Personne);
    }

    @Override
    public Optional<Personne> readOne(Long id) {
        return personneRepository.findById(id);
    }

    @Override
    public List<Personne> readAll() {
        return personneRepository.findAll();
    }
}

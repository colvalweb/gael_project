package ca.qc.colval.examen1.Controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Controller
public class indexController {

    public indexController() {

    }

    @GetMapping("/")
    public String index(Model model) {
        String name = "GAEL ONOUK";
        String date = LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd-MM-yyyy"));
        model.addAttribute("Name", name);
        model.addAttribute("Date", date);
        return "index/index";
    }
}

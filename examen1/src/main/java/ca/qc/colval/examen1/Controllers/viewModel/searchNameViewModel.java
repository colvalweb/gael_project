package ca.qc.colval.examen1.Controllers.viewModel;

public class searchNameViewModel {
    private String firstname;

    public searchNameViewModel(String firstname) {
        this.firstname = firstname;
    }

    public searchNameViewModel() {
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    @Override
    public String toString() {
        return "searchNameViewModel {" +
                "name='" + firstname + '\'' +
                '}';
    }

}

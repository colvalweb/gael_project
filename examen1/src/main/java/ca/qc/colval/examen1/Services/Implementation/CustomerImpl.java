package ca.qc.colval.examen1.Services.Implementation;

import ca.qc.colval.examen1.Models.Entities.Customer;
import ca.qc.colval.examen1.Repositories.CustomerRepository;
import ca.qc.colval.examen1.Services.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CustomerImpl implements CustomerService {

    private final CustomerRepository customerRepository;

    @Autowired
    public CustomerImpl(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @Override
    public Customer create(Customer customer) {
        return customerRepository.save(customer);
    }

    @Override
    public Optional<Customer> readOne(Long id) {
        return customerRepository.findById(id);
    }

    @Override
    public List<Customer> readAll() {
        return customerRepository.findAll().stream().limit(50).collect(Collectors.toList());
    }

    @Override
    public void delete(Long id) {
        customerRepository.deleteById(id);
    }


    @Override
    public List<Customer> getCustomerDetailsByIdLimitFifty(long customerId) {
        return customerRepository.getCustomerDetailsById(customerId).stream().limit(50).collect(Collectors.toList());
    }

    @Override
    public List<Customer> getSimilarFirstLastName(String subStr) {
        return customerRepository.getSimilarFirstLastName(subStr);
    }

}

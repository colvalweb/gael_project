package ca.qc.colval.examen1.Controllers;

import ca.qc.colval.examen1.Controllers.viewModel.searchNameViewModel;
import ca.qc.colval.examen1.Models.Entities.Customer;
import ca.qc.colval.examen1.Services.Implementation.CustomerImpl;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/customer")
public class customerController {
    CustomerImpl customerImpl;

    public customerController(CustomerImpl customerImpl) {
        this.customerImpl = customerImpl;
    }

    @GetMapping("/all")
    public String customer(Model model) {
        model.addAttribute("allCustomer", customerImpl.readAll());
        model.addAttribute("customerCount", customerImpl.readAll().size());
        return "customer/customer";
    }

    @PostMapping("/doSearchNames")
    public String searchCustomerByName(searchNameViewModel searchNameViewModel, Model model) {

        List<Customer> customerWithName = customerImpl.getSimilarFirstLastName(searchNameViewModel.getFirstname());
        int nbCustomer = customerWithName.size();
        model.addAttribute("searchNames", new searchNameViewModel(searchNameViewModel.getFirstname()));
        model.addAttribute("allCustomer", customerWithName);
        model.addAttribute("customerCount", nbCustomer);
        return "customer/customer";
    }

    @GetMapping("/{id}")
    public String getCustomer(Model model, @PathVariable long id) {

        Optional<Customer> customer = customerImpl.readOne(id);

        model.addAttribute("customer", customer.get());

        return "customer/customerdetail";

    }
}

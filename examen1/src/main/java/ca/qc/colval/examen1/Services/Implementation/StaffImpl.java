package ca.qc.colval.examen1.Services.Implementation;

import ca.qc.colval.examen1.Models.Entities.Staff;
import ca.qc.colval.examen1.Repositories.StaffRepository;
import ca.qc.colval.examen1.Services.StaffService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class StaffImpl implements StaffService {
    private final StaffRepository staffRepository;

    @Autowired
    public StaffImpl(StaffRepository staffRepository) {
        this.staffRepository = staffRepository;
    }


    @Override
    public Optional<Staff> readOne(Long staffId) {
        return staffRepository.findById(staffId);
    }

    @Override
    public List<Staff> readAll() {
        return staffRepository.findAll();
    }

    @Override
    public void delete(Long staffId) {
        staffRepository.deleteById(staffId);
    }


    @Override
    public List<Staff> getStaffByStoreLimitFifty(long storeId) {
        return staffRepository.getStaffByStore(storeId).stream().limit(50).collect(Collectors.toList());
    }
}

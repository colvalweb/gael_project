package ca.qc.colval.examen1.Services;

import ca.qc.colval.examen1.Models.Entities.Staff;

import java.util.List;
import java.util.Optional;

public interface StaffService {

    Optional<Staff> readOne(Long staffId);

    List<Staff> readAll();

    void delete(Long staffId);

    List<Staff> getStaffByStoreLimitFifty(long storeId);
}

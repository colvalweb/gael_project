package ca.qc.colval.examen1.Controllers;

import ca.qc.colval.examen1.Services.Implementation.CustomerImpl;
import ca.qc.colval.examen1.Services.Implementation.StaffImpl;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/staff")
public class staffController {
    StaffImpl staffImpl;

    public staffController(StaffImpl staffImpl) {
        this.staffImpl = staffImpl;
    }

    @GetMapping("/all")
    public String staff(Model model) {
        model.addAttribute("all", staffImpl.readAll());

        return "staff/staff";
    }
}

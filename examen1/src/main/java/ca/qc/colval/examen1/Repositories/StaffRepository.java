package ca.qc.colval.examen1.Repositories;

import ca.qc.colval.examen1.Models.Entities.Staff;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface StaffRepository extends JpaRepository<Staff, Long> {
    List<Staff> getStaffByStore(long storeId);
}

package ca.qc.colval.examen1.Services;

import ca.qc.colval.examen1.Models.Entities.Customer;

import java.util.List;
import java.util.Optional;

public interface CustomerService {
    Customer create(Customer customer);

    Optional<Customer> readOne(Long id);

    List<Customer> readAll();

    void delete(Long id);

    List<Customer> getCustomerDetailsByIdLimitFifty(long customerId);

    List<Customer> getSimilarFirstLastName(String subStr);


}
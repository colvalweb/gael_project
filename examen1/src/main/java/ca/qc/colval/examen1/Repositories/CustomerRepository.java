package ca.qc.colval.examen1.Repositories;

import ca.qc.colval.examen1.Models.Entities.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CustomerRepository extends JpaRepository<Customer, Long> {
    List<Customer> getCustomerDetailsById(long customerId);

    List<Customer> getSimilarFirstLastName(String substr);

    List<Customer> getCustomerDetailsByDate(java.sql.Timestamp date);


}